#include "TestApplication.h"

#include <nox/app/resource/cache/LruCache.h>
#include <nox/app/resource/provider/BoostFilesystemProvider.h>
#include <nox/app/resource/Handle.h>
#include <nox/app/resource/loader/OggLoader.h>
#include <nox/app/resource/loader/JsonLoader.h>
#include <nox/app/resource/data/JsonExtraData.h>
#include <nox/app/audio/PlaybackProcess.h>
#include <nox/app/audio/openal/OpenALSystem.h>
#include <nox/app/storage/DataStorageBoost.h>
#include <nox/logic/event/Manager.h>
#include <nox/logic/physics/box2d/Box2DSimulation.h>
#include <nox/logic/world/Manager.h>

#include <nox/logic/actor/component/Transform.h>
#include <nox/logic/physics/actor/ActorPhysics.h>
#include <nox/logic/physics/actor/ActorGravitation.h>
#include <nox/logic/graphics/actor/ActorSprite.h>


TestApplication::TestApplication():
    nox::app::SdlApplication("Network Client Demo", "OptNOX"),
    logic(nullptr),
    world(nullptr),
    netView(nullptr)
{

}

bool TestApplication::onInit()
{
    nox::app::SdlApplication::onInit();

    auto logic = std::make_unique<nox::logic::Logic>();
    this->logic = logic.get();
    this->addProcess(std::move(logic));

    auto physics = std::make_unique<nox::logic::physics::Box2DSimulation>(this->logic);
    this->logic->setPhysics(std::move(physics));

    auto world = std::make_unique<nox::logic::world::Manager>(this->logic);
    this->world = world.get();
    this->logic->setWorldHandler(std::move(world));

    auto cnet = std::make_unique<nox::app::net::ClientNetworkView>(&pktTrans);
    this->netView = cnet.get();
    this->logic->addView(std::move(cnet));

    return true;
}

void TestApplication::onDestroy()
{
    nox::app::SdlApplication::onDestroy();
}
