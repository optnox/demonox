#include "TestApplication.h"


int main(int argc, char *argv[])
{
    TestApplication app;

    if (!app.init(argc, argv))
    {
        return 1;
    }

    const int ret = app.execute();
    app.shutdown();

    return ret;
}
