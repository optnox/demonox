#ifndef TESTAPPLICATION_H_
#define TESTAPPLICATION_H_

#include <nox/common/types.h>
#include <nox/util/process/Manager.h>
#include <nox/app/SdlApplication.h>
#include <nox/app/log/Logger.h>
#include <nox/logic/Logic.h>
#include <nox/logic/View.h>
#include <nox/logic/event/IListener.h>
#include <nox/logic/event/ListenerManager.h>
#include <nox/logic/world/Manager.h>
#include <nox/util/Timer.h>

#include <nox/app/net/ClientNetworkView.h>
#include <nox/app/net/ClientNetworkView.h>
#include <nox/app/net/IPacketTranslator.h>
#include <nox/app/net/Packet.h>



class PacketTranslator: public nox::app::net::IPacketTranslator
{
public:
    std::shared_ptr<nox::logic::event::Event> 
        translatePacket(const nox::app::net::Packet *packet) override
    {
        return nullptr;
    }
};


class TestApplication: public nox::app::SdlApplication
{
public:
    TestApplication();

private:
    nox::logic::Logic *logic;
    nox::logic::world::Manager *world;
    nox::app::net::ClientNetworkView *netView;
    PacketTranslator pktTrans;


    bool onInit() override;
    void onDestroy() override;
};


#endif
