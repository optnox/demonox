DemoNox
=======

This repository contains demonstrative examples built for the NOX engine,
along with a suite for managing and executing them.

Get started by executing the "demo" script. It will guide you through the
process of setting everything up nice and tidily.


SETUP
=====

**Unix**

No real setup is required. Run the "demo" script and follow the instructions
as they appear.


**Windows**

The demo script must initially be run from PowerShell with administrative
privileges. After this initial execution, a regular PowerShell should be
used. It's imperative that "git" exists somewhere on the PATH-variable.
GitShell (bundled with GitHub for Windows) is the only tested shell. If
other variants doesn't work, use GitShell - it appears to have GNU CoreUtils
aliasing (I can't be bothered to figure out their Windows equivalent). Download
the GitHub for Windows package [here](https://windows.github.com/).

Follow the setup instructions provided by Magnus 
[here](https://bitbucket.org/suttungdigital/nox-engine/src/master/README.md).
It is **REQUIRED** that you have assigned the system variable 
*CMAKE_PREFIX_PATH* to point at the *".../usr"* directory. The DLL binaries
should also be available on the *PATH* sysvar.

Obviously, Perl is required. 
[This](http://www.activestate.com/activeperl/downloads) build works just dandy.
