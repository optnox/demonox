cmake_minimum_required(VERSION 2.8.11)
project(nox-sample)

set(NOX_LIB_DIR "" CACHE STRING "The directory containing the NOX Library.")
set(NOX_INC_DIR "" CACHE STRING "The directory containing the NOX header files")
set(OUTPUT_EXE "nox-demo" CACHE STRING "The executable output file")

set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "${CMAKE_CURRENT_SOURCE_DIR}/../../.cmake_modules/")

if ("${NOX_LIB_DIR}" STREQUAL "" OR "${NOX_LIB_DIR}" STREQUAL "OFF") 
    message(FATAL_ERROR "NOX_LIB_DIR is undefined.")
endif()

if ("${NOX_INC_DIR}" STREQUAL "" OR "${NOX_INC_DIR}" STREQUAL "OFF") 
    message(FATAL_ERROR "NOX_INC_DIR is undefined.")
endif()

if ("${OUTPUT_EXE}" STREQUAL "nox-demo")
    message(WARNING "The output executable should have a custom defined name")
endif()

set(NOX_BUILD_STATIC 1)
set(NOX_RESOURCE_JSON 1)
set(NOX_PHYSICS_BOX2D 1)
add_subdirectory(third_party)


###########################################
##  SUTTUNG NOX CMAKE LIBRARY DISCOVERY  ##
###########################################

# NOX_DEPEND_BOOST_FILESYSTEM
list(APPEND NOX_BOOST_COMPONENTS system filesystem)


# NOX_DEPEND_BOOST
set(Boost_USE_STATIC_LIBS OFF)
set(Boost_USE_STATIC_RUNTIME OFF)

find_package(Boost REQUIRED COMPONENTS ${NOX_BOOST_COMPONENTS})

# Force dynamic linking rather than the default static linking, and disable autolink.
list(APPEND NOX_COMPILE_DEFINITIONS -DBOOST_ALL_DYN_LINK -DBOOST_ALL_NO_LIB)
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${Boost_INCLUDE_DIRS})
list(APPEND NOX_EXTERNAL_LIBRARIES ${Boost_LIBRARIES})


# NOX_DEPEND_JSONCPP
list(APPEND NOX_EXTERNAL_LIBRARIES jsoncpp_lib)
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:jsoncpp_lib,INTERFACE_INCLUDE_DIRECTORIES>)


# NOX_DEPEND_OGG
find_package(OGG REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${OGG_LIBRARIES})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${OGG_INCLUDE_DIR})


# NOX_DEPEND_VORBIS_FILE
find_package(VorbisFile REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${VORBISFILE_LIBRARIES})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${VORBISFILE_INCLUDE_DIR})


# NOX_DEPEND_OPENAL
find_package(OpenAL REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${OPENAL_LIBRARY})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${OPENAL_INCLUDE_DIR})


# NOX_DEPEND_SDL2
find_package(SDL2 REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${SDL2_LIBRARY})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${SDL2_INCLUDE_DIR})


#NOX_DEPEND_SDL2_NET
find_package(SDL2_net REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${SDL2_NET_LIBRARY})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${SDL2_NET_INCLUDE_DIR})


# NOX_DEPEND_SDL2_IMAGE
find_package(SDL2_image REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${SDL2_IMAGE_LIBRARIES})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${SDL2_IMAGE_INCLUDE_DIRS})


# NOX_DEPEND_OPENGL 
find_package(OpenGL REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${OPENGL_LIBRARY})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${OPENGL_INCLUDE_DIR})


# NOX_DEPEND_GLEW
find_package(GLEW REQUIRED)
list(APPEND NOX_EXTERNAL_LIBRARIES ${GLEW_LIBRARIES})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${GLEW_INCLUDE_DIRS})


# NOX_DEPEND_BOX2D
list(APPEND NOX_EXTERNAL_LIBRARIES Box2D)
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:Box2D,INTERFACE_INCLUDE_DIRECTORIES>)


# NOX_DEPEND_POLY2TRI
list(APPEND NOX_EXTERNAL_LIBRARIES poly2tri-static)
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS $<TARGET_PROPERTY:poly2tri-static,INTERFACE_INCLUDE_DIRECTORIES>)


# NOX_DEPEND_GLM
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${NOX_GLM_INCLUDE_DIR})


###############################
##  COMPILER IDENTIFICATION  ##
###############################

if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU" AND CMAKE_CXX_COMPILER_VERSION VERSION_LESS 4.8)
		message(WARNING "GCC versions below 4.8 might not be able to build because of missing C++11 support.")
	endif()

	include(CheckCXXCompilerFlag)

	set(HAS_CXX14 OFF)

	set(CXX14_FLAG -std=c++14)
	check_cxx_compiler_flag(${CXX14_FLAG} HAS_CXX14_FLAG)

	if (HAS_CXX14_FLAG)
		set(HAS_CXX14 ON)
	elseif (NOT HAS_CXX14)
		set(CXX14_FLAG -std=c++1y)
		check_cxx_compiler_flag(${CXX14_FLAG} HAS_CXX1Y_FLAG)
	endif ()

	if (HAS_CXX1Y_FLAG)
		set(HAS_CXX14 ON)
	elseif (NOT HAS_CXX14)
		set(CXX14_FLAG "-std=c++1y -stdlib=libc++")
		check_cxx_compiler_flag(${CXX14_FLAG} HAS_CXX1Y_STDLIB_FLAG)
	endif ()

	if (HAS_CXX1Y_STDLIB_FLAG)
		set(HAS_CXX14 ON)
	endif ()

	if (HAS_CXX14)
		set(NOXSAMPLE_CXX_FLAGS "${NOXSAMPLE_CXX_FLAGS} ${CXX14_FLAG}")
		message(STATUS "Setting c++ standard flag to ${CXX14_FLAG}")
	else ()
		message(FATAL_ERROR "Compiler does not support C++14 (${CXX14_FLAG})")
	endif ()

	check_cxx_compiler_flag(-fdiagnostics-color HAS_COLOR_OUTPUT)
	if (HAS_COLOR_OUTPUT)
		set(NOXSAMPLE_CXX_FLAGS "${NOXSAMPLE_CXX_FLAGS} -fdiagnostics-color=auto")
	endif ()

	check_cxx_compiler_flag(-flto HAS_LTO)
	check_cxx_compiler_flag(-Ofast HAS_OPTIMIZE_FAST)
	check_cxx_compiler_flag(-ffinite-math-only HAS_FINITE_MATH)
	check_cxx_compiler_flag(-s HAS_STRIP_SYMBOLS)

	if (HAS_LTO)
		message(STATUS "Enabling link-time optimizations (-flto)")
		set(NOXSAMPLE_CXX_FLAGS_RELEASE "${NOXSAMPLE_CXX_FLAGS_RELEASE} -flto")
		set(NOXSAMPLE_LINKER_FLAGS_RELEASE "${NOXSAMPLE_LINKER_FLAGS_RELEASE} -flto")

		if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
			# Need to use gcc-ar and gcc-ranlib for LTO with static libraries.
			# See http://gcc.gnu.org/gcc-4.9/changes.html
			set(CMAKE_AR gcc-ar)
			set(CMAKE_RANLIB gcc-ranlib)
		endif ()
	endif ()

	if (HAS_OPTIMIZE_FAST)
		message(STATUS "Enabling -Ofast optimization.")
		set(NOXSAMPLE_CXX_FLAGS_RELEASE "${NOXSAMPLE_CXX_FLAGS_RELEASE} -Ofast")
	endif ()

	if (HAS_FINITE_MATH)
		message(STATUS "Enabling -ffinite-math-only optimization.")
		set(NOXSAMPLE_CXX_FLAGS_RELEASE "${NOXSAMPLE_CXX_FLAGS_RELEASE} -ffinite-math-only")
	endif ()

	if (HAS_STRIP_SYMBOLS)
		message(STATUS "Stripping symbols (-s)")
		set(NOXSAMPLE_LINKER_FLAGS_RELEASE "${NOXSAMPLE_LINKER_FLAGS_RELEASE} -s")
	endif ()
elseif ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "MSVC")
	if (NOT CMAKE_LIBRARY_ARCHITECTURE)
		if (CMAKE_SIZEOF_VOID_P EQUAL 8)
			set(CMAKE_LIBRARY_ARCHITECTURE "x64")
		else ()
			set(CMAKE_LIBRARY_ARCHITECTURE "x86")
		endif ()
	endif ()

	set(NOXSAMPLE_CXX_FLAGS "${NOXSAMPLE_CXX_FLAGS} /MP")
else ()
	message(WARNING "You are using an unsupported compiler. Compilation has only been tested with GCC.")
endif ()

# Include all files under the src-directory
file(GLOB SOURCES "src/*.h" "src/*.cpp")

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${NOXSAMPLE_CXX_FLAGS}")
set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} ${NOXSAMPLE_CXX_FLAGS_RELEASE}")
set(CMAKE_EXE_LINKER_FLAGS_RELEASE "${CMAKE_EXE_LINKER_FLAGS_RELEASE} ${NOXSAMPLE_LINKER_FLAGS_RELEASE}")

add_executable(${OUTPUT_EXE} ${SOURCES})
set_target_properties(${OUTPUT_EXE}
    PROPERTIES
    ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    LIBRARY_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
    RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}"
)

# Link with the customized NOX library
find_library(NOX_LIBRARY nox ${NOX_LIB_DIR})

list(APPEND NOX_EXTERNAL_LIBRARIES ${NOX_LIBRARY})
list(APPEND NOX_EXTERNAL_INCLUDE_DIRS ${NOX_INC_DIR})

target_link_libraries(${OUTPUT_EXE} ${NOX_EXTERNAL_LIBRARIES})
target_include_directories(${OUTPUT_EXE} SYSTEM PUBLIC ${NOX_EXTERNAL_INCLUDE_DIRS})
target_compile_definitions(${OUTPUT_EXE} PUBLIC ${NOX_COMPILE_DEFINITIONS})

